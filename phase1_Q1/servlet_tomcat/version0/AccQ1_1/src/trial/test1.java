package trial;

import java.lang.Math;

public class test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(decipherPDC("URYYBBJEX",13)); 
		System.out.println(decipherPDC("ABDGCEHKFILNJMOP",23)); 
		System.out.println(decipherPDC("ABCD",23));
		
	}
	
	private static String decipherPDC(String diagMsg, int minikey){
		minikey = minikey % 26;
		int diameter = (int) Math.sqrt(diagMsg.length());
		int i,j,k;
		String msg = ""+diagMsg.charAt(0);
		String finalmsg = "";
		int tmpAscii;
		char tmpChar = 65;
		for(i=2;i<=diameter;i++)
		{
			for(j=i;j>0;j--)
			{	
				k = i-j;
				msg = msg + diagMsg.charAt(k*diameter+j-1);
			}
		}
		// after passing the diameter of the matrix of encoded message
		for(i=diameter-1;i>=1;i--)
		{
			for(j=diameter;j>(diameter-i);j--)
			{	
				k = 2*diameter-i-j;
				msg = msg + diagMsg.charAt(k*diameter+j-1);
			}
		}
		
		msg = msg.toUpperCase(); // make sure the string is uppercase
		
		// get the final message with minikey
		for(i=0;i<msg.length();i++)
		{
			tmpAscii = (int) msg.charAt(i);
			if(tmpAscii-minikey<65)
				tmpAscii += 26;
			tmpChar = (char) (tmpAscii-minikey);
			finalmsg = finalmsg + tmpChar;
		}
		return finalmsg;
	}

}
