package accelerator;

import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.*;

public class ServletListener implements ServletContextListener {
	private ServletContext context = null; 
	// use a ConcurrentHashMap to provide a global hashmap for all servlet threads
	private  ConcurrentHashMap<String, Integer> q1hMap = null;  
	
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		this.q1hMap = new ConcurrentHashMap<String, Integer>();
		this.context.setAttribute("Q1_MAP", q1hMap);
	}
	
	public void contextDestroyed(ServletContextEvent event) {
		this.context = null;
		this.q1hMap = null;
	}
}
