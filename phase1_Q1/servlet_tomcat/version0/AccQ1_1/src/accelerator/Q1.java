package accelerator;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Q1 extends HttpServlet {
	
	private BigInteger secretKey = new BigInteger(
			"8271997208960872478735181815578166723519929177896558845922250595511921395049126920528021164569045773");
    private String teamId = new String("accelerator");
    private String teamAWSId = new String("7350-7169-5556");
    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException{
			String inputKey = request.getParameter("key");
			Integer miniKey;
			
			// use a global concurrent hash map to reduce the number of the calculations of BigInteger
			@SuppressWarnings("unchecked")
			ConcurrentHashMap<String, Integer> q1hMap = 
	        	(ConcurrentHashMap<String, Integer>) getServletContext().getAttribute("Q1_MAP");
			
			
			miniKey = q1hMap.get(inputKey);
			if(miniKey == null){
				BigInteger keyY = new BigInteger(inputKey);
				miniKey = keyY.divide(secretKey).mod(new BigInteger("25")).add(new BigInteger("1")).intValue(); // get Z
		        q1hMap.put(inputKey, miniKey);  
			}
			
	        String finalMsg = decipherPDC(request.getParameter("message"), miniKey); // get the final message with diagonal message
	        
			// output
			PrintWriter writer = response.getWriter(); 
	        writer.println(teamId + "," + teamAWSId);
	       
	        Date date = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-5"));
	        
			writer.println(simpleDateFormat.format(date));
	        writer.println(finalMsg); // the result of the decoding
	        
	        writer.close();
			}
	
	private String decipherPDC(String diagMsg, int minikey){
		minikey = minikey % 26;
		int diameter = (int) Math.sqrt(diagMsg.length());
		int i,j,k;
		String msg = ""+diagMsg.charAt(0);
		String finalmsg = "";
		int tmpAscii;
		char tmpChar = 65;
		for(i=2;i<=diameter;i++)
		{
			for(j=i;j>0;j--)
			{	
				k = i-j;
				msg = msg + diagMsg.charAt(k*diameter+j-1);
			}
		}
		// after passing the diameter of the matrix of encoded message
		for(i=diameter-1;i>=1;i--)
		{
			for(j=diameter;j>(diameter-i);j--)
			{	
				k = 2*diameter-i-j;
				msg = msg + diagMsg.charAt(k*diameter+j-1);
			}
		}
		
		msg = msg.toUpperCase(); // make sure the string is uppercase
		
		// get the final message with minikey
		for(i=0;i<msg.length();i++)
		{
			tmpAscii = (int) msg.charAt(i);
			if(tmpAscii-minikey<65)
				tmpAscii += 26;
			tmpChar = (char) (tmpAscii-minikey);
			finalmsg = finalmsg + tmpChar;
		}
		return finalmsg;
	}
}
