package accelerator;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
//import java.util.TimeZone;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Q1 extends HttpServlet {
	
	private BigInteger secretKey = new BigInteger(
			"8271997208960872478735181815578166723519929177896558845922250595511921395049126920528021164569045773");
    private String teamId = new String("accelerator");
    private String teamAWSId = new String("7350-7169-5556");

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException{
		String inputKey = request.getParameter("key");
		Integer miniKey;
		
		
		// use a global hash map to reduce the number of the calculations of BigInteger
		@SuppressWarnings("unchecked")
		HashMap<String, Integer> q1hMap = 
	    	(HashMap<String, Integer>) getServletContext().getAttribute("Q1_MAP");

	    @SuppressWarnings("unchecked")
		HashMap<Integer, ArrayList<Integer>> q1IdxMap = 
	    	(HashMap<Integer, ArrayList<Integer>>) getServletContext().getAttribute("Q1_IDX_MAP");

			
		miniKey = q1hMap.get(inputKey);
		if(miniKey == null){
			BigInteger keyY = new BigInteger(inputKey);
			miniKey = keyY.divide(secretKey).mod(new BigInteger("25")).add(new BigInteger("1")).intValue(); // get Z
	        q1hMap.put(inputKey, miniKey);  
		}
		
        String finalMsg = decipherPDC(request.getParameter("message"), miniKey,q1IdxMap); // get the final message with diagonal message
        
		// output
		PrintWriter writer = response.getWriter(); 
        writer.println(teamId + "," + teamAWSId);
       
        Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        
		writer.println(simpleDateFormat.format(date));
        writer.println(finalMsg); // the result of the decoding
        
        writer.close();
	}
	
	private String decipherPDC(String diagMsg, int minikey,HashMap<Integer, ArrayList<Integer>> q1IdxMap){
		minikey = minikey % 26;
		int len = diagMsg.length();
		
		int diameter = (int) Math.sqrt(len);
		char[] diagMsgStr = diagMsg.toCharArray();
		
		int i,j,k;
		
		//String msg = ""+diagMsg.charAt(0);
		char[] result = new char[len];
		result[0] = diagMsgStr[0];
		
		
		String finalmsg = "";
		char tmpChar = 65;
		
		List<Integer> idxList = q1IdxMap.get(diameter);

		for(i=1;i<len;i++){
			result[i] = diagMsgStr[idxList.get(i)];
		}
		
		// get the final message with minikey
		for(i=0;i<len;++i)
		{
			if(result[i]-minikey<65)
				result[i] += 26;
			result[i] = (char) (result[i]-minikey);
		}
		return new String(result);
	}
}
