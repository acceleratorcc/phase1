package accelerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.*;

public class ServletListener implements ServletContextListener {
	private ServletContext context = null; 
	// use a ConcurrentHashMap to provide a global hashmap for all servlet threads
	private HashMap<String, Integer> q1hMap = null;
	private HashMap<Integer, ArrayList<Integer>> q1IdxMap = null;
	
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		this.q1hMap = new HashMap<String, Integer>();
		this.q1IdxMap = new HashMap<Integer, ArrayList<Integer>>();

		initMsgMap(20);

		this.context.setAttribute("Q1_MAP", q1hMap);
		this.context.setAttribute("Q1_IDX_MAP", q1IdxMap);
	}
	
	public void contextDestroyed(ServletContextEvent event) {
		this.context = null;
		this.q1hMap = null;
		this.q1IdxMap = null;
	}

	/**
	 * Produces the deciphering sequences (index array) of cipher matrices with sizes 
	 * (size: number of elements) from 2 to the diameterUpperBound 
	 * (diameter: as row and column has the same length, diameter stands for either length)
	 * @param diameterUpperBound
	 */
	private void initMsgMap(int diameterUpperBound)
	{
		ArrayList<Integer> tmpArr = new ArrayList<Integer>();
		int ite;
		for (ite=2;ite<=diameterUpperBound;ite++){
			this.q1IdxMap.put(ite,getIdxArrWithDiameter(ite));
		}
	}

	/**
	 * Produces the index array (deciphering sequence) of the cipher matrix and put the array into the global msgMap
	 * @param diameter: the diameter of the matrix (as row and column has the same length, diameter stands for either length)
	 * @return ArrayList<Integer>: the index array (deciphering sequence)
	 */
	private ArrayList<Integer> getIdxArrWithDiameter(int diameter)
	{
		ArrayList<Integer> tmpArr = new ArrayList<Integer>();
		tmpArr = new ArrayList<Integer>();
		int i,j,k;
		tmpArr.add(0);
		for(i=2;i<=diameter;i++)
		{
			for(j=i;j>0;j--)
			{	
				k = i-j;
				tmpArr.add(k*diameter+j-1);
			}
		}
		// after passing the diameter of the matrix of encoded message
		for(i=diameter-1;i>=1;i--)
		{
			for(j=diameter;j>(diameter-i);j--)
			{	
				k = 2*diameter-i-j;
				tmpArr.add(k*diameter+j-1);
			}
		}
		return tmpArr;
	}

}
