#!/bin/bash

# Please sudo chmod +x ./auto.sh and run with sudo 
# ========================================================
# install java

sudo apt-get update
sudo apt-get --yes --force-yes install default-jre
sudo apt-get --yes --force-yes install default-jdk

# java should be installed in /usr/lib/jvm/default-java
# ========================================================


# auto config script for Tomcat8 on Ubuntu
cd ~

# download binary 
wget http://www.us.apache.org/dist/tomcat/tomcat-8/v8.0.28/bin/apache-tomcat-8.0.28.tar.gz

# untar and move to destination folder
tar xvzf apache-tomcat-8.0.28.tar.gz

# install in /opt/tomcat (sudo)
sudo mv apache-tomcat-8.0.28 /opt/tomcat

# modify bashrc (if entry not existed)
if [[ `echo $JAVA_HOME` ]]; then
    echo "JAVA_HOME exists in bashrc"
else
    echo 'export JAVA_HOME=/usr/lib/jvm/default-java' >> ~/.bashrc
fi


if [[ `echo $CATALINA_HOME` ]]; then
    echo "CATALINA_HOME exists in bashrc"
else
    echo 'export CATALINA_HOME=/opt/tomcat' >> ~/.bashrc
fi


# do the following manually
# restart bashrc to make effect
#. ~/.bashrc

# start tomcat
#sudo sh $CATALINA_HOME/bin/startup.sh








