package vertex;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.*;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Lily
 * @team   accelerator
 * 
 * Improvement :
 * Use 2D array to replace Map<int,list<int>> for index map 
 *
 */
public class Q1Verticle extends AbstractVerticle {
	private BigInteger secretKey = new BigInteger(
			"8271997208960872478735181815578166723519929177896558845922250595511921395049126920528021164569045773");
    private String teamId = new String("accelerator");
    private String teamAWSId = new String("7350-7169-5556");

    // Key: inputKey from the GET query. 
    // Value: the integer value of the minikey produced from the inputKey
	private HashMap<String, Integer> keyMap = new HashMap<String, Integer>();

	// Key: the size(nubmer of elements) of the cipher matrix array (diameter^2)
	// Value: the index array (deciphering sequence) of matrix array
	private static int[][] idxMap = initIdxMap(25);

	
	@Override
	public void start(Future<Void> fut) 
	{
		HttpServerOptions options = new HttpServerOptions().setMaxWebsocketFrameSize(10000000);
		HttpServer server = vertx.createHttpServer(options);

		server.requestHandler(r -> {

					String inputKey = r.getParam("key");
					String msg = r.getParam("message");

					String finalMsg;

					if (inputKey==null||msg==null)
						finalMsg = "\n";
					else{
						Integer miniKey;
						miniKey = keyMap.get(inputKey);
						if(miniKey == null){
							BigInteger keyY = new BigInteger(inputKey);
							miniKey = keyY.divide(secretKey).mod(new BigInteger("25")).add(new BigInteger("1")).intValue(); // get Z
							keyMap.put(inputKey, miniKey);  
						}

						// get the final message with diagonal message and the minikey
						finalMsg = decipherPDC(r.getParam("message"), miniKey); 
					}
					
					Date date = new Date();
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					// response
					r.response().end(teamId + "," + teamAWSId+"\n"+simpleDateFormat.format(date)+"\n"+finalMsg+"\n");
				})
//				.listen(80);
				.listen(80, result -> {
					if (result.succeeded()) {
						fut.complete();
					} else {
						fut.fail(result.cause());
					}
				});
	}


	private String decipherPDC(String diagMsg, int minikey){
		minikey = minikey % 26;
		int len = diagMsg.length();
		
		int diameter = (int) Math.sqrt(len);
		char[] diagMsgStr = diagMsg.toCharArray();
		
		int i,j,k;
		
		//String msg = ""+diagMsg.charAt(0);
		char[] result = new char[len];
		result[0] = diagMsgStr[0];
		
		
		String finalmsg = "";
		char tmpChar = 65;
		
		int[] idxList = idxMap[diameter];

		for(i=1;i<len;i++){
			result[i] = diagMsgStr[idxList[i]];
		}
		
		// get the final message with minikey
		for(i=0;i<len;++i)
		{
			if(result[i]-minikey<65)
				result[i] += 26;
			result[i] = (char) (result[i]-minikey);
		}
		return new String(result);
	}
	

	/**
	 * Produces the deciphering sequences (index array) of cipher matrices with sizes 
	 * (size: number of elements) from 2 to the diameterUpperBound 
	 * (diameter: as row and column has the same length, diameter stands for either length)
	 * 
	 * http://stackoverflow.com/questions/17997600/multidimensional-arrays-with-different-sizes
	 * 
	 * @param diameterUpperBound
	 */
	private static int[][]  initIdxMap(int diameterUpperBound)
	{
		int[][] map = new int[diameterUpperBound+1][];
		for (int i=1;i<=diameterUpperBound;++i){
			map[i] = getIdxArrWithDiameter(i);
		}
		return map;
	}

	/**
	 * Produces the index array (deciphering sequence) of the cipher matrix and put the array into the global msgMap
	 * @param diameter: the diameter of the matrix (as row and column has the same length, diameter stands for either length)
	 * @return ArrayList<Integer>: the index array (deciphering sequence)
	 */
	private static int[] getIdxArrWithDiameter(int diameter)
	{
		int[] idxArr = new int[diameter*diameter];
		idxArr[0] = 0;
		
		int i,j,k,idx=1;
		for(i=2;i<=diameter;i++)
		{
			for(j=i;j>0;j--)
			{	
				k = i-j;
				idxArr[idx] = k*diameter+j-1;
				++idx;
			}
		}
		// after passing the diameter of the matrix of encoded message
		for(i=diameter-1;i>=1;i--)
		{
			for(j=diameter;j>(diameter-i);j--)
			{	
				k = 2*diameter-i-j;
				idxArr[idx] = k*diameter+j-1;
				++idx;
			}
		}
		return idxArr;
	}

	
}