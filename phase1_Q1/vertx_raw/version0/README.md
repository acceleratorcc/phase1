# Accelerator Phase1

## TLDR
+ `mvn clean compile`
+ `mvn clean package`
+ Run with: `java -jar target/ccquery1-1.0-fat.jar`
+ Now you are good to go!

## How to build and run this particular project?
We are using **maven** here, be sure to have [maven installed](https://maven.apache.org/install.html) on your machine. 

The **pom.xml** is written and tested before submit.

Here are the steps to get this project running (Make sure you are under the **same directory where lies pom.xml**):

+ `mvn clean compile`
+ `mvn clean package`
+ Run with: `java -jar target/artifactId-version-fat.jar`
	+ (artifactId and version are defined in pom.xml)
	+ (for query1's version1, simply run: `java -jar target/ccquery1-1.0-fat.jar`)

## TODO
+ As stated in the link below ([Stackoverflow: Does Vert.x has real concurrency for single verticles?](http://stackoverflow.com/questions/17787339/does-vert-x-has-real-concurrency-for-single-verticles)), running multiple verticle instances might elevate the throughput, if the queries do not take up the whole cpu.
	+ Study the *Creating a clustered Vert.x object* part (and beyond) of [Vert.x core documentation](http://vertx.io/docs/vertx-core/java/).
+ About Verticles
	+ This link includes a full coverage (including event bus). [Vert.x Verticles](http://tutorials.jenkov.com/vert.x/verticles.html)

## Understanding & Using Vert.x
+ Vert.x has a solid documentation and many a [example](https://github.com/vert-x3/vertx-examples/tree/master/core-examples). Check them out.
+ To start with, study the first blog. Make sure you understand how maven's pom.xml works and how the dependencies and directory are architected.
	+ Or [this simple example here.](https://github.com/vert-x3/vertx-examples/tree/master/maven-simplest)
+ [Stackoverflow: Does Vert.x has real concurrency for single verticles?](http://stackoverflow.com/questions/17787339/does-vert-x-has-real-concurrency-for-single-verticles)
	+ Read this. Note this: "start the vert.x platform using the -instance parameter which defines how many instances of a given verticle are run. Vert.x does a bit of magic under the hood so that 10 instances of my server do not try to open 10 server sockets but actually a single on instead. This way vert.x is horizontally scalable even for single verticles."

### Concurrency
+ Understanding the concurrency in Vert.x [Development with Vert.x: an event-driven application framework for the JVM ](http://www.slideshare.net/wuman/development-with-vertx-an-eventdriven-application-framework-for-the-jvm)
+ A usecase of ConcurrentHashMap [Github: muraken720/vertx-test-example](https://github.com/muraken720/vertx-test-example/blob/master/src/test/java/muraken720/vertx/mod/testexample/integration/java/BusModVerticleTest.java)