package io.vertx.accelerator;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author VR
 * @team   accelerator
 *
 */
public class Q1Verticle extends AbstractVerticle {
	private BigInteger secretKey = new BigInteger(
			"8271997208960872478735181815578166723519929177896558845922250595511921395049126920528021164569045773");
    private String teamId = new String("accelerator");
    private String teamAWSId = new String("7350-7169-5556");

    // Key: inputKey from the GET query. 
    // Value: the integer value of the minikey produced from the inputKey
	private HashMap<String, Integer> keyMap = new HashMap<String, Integer>();

	// Key: the size(nubmer of elements) of the cipher matrix array (diameter^2)
	// Value: the index array (deciphering sequence) of matrix array
	private HashMap<Integer, ArrayList<Integer>>  msgMap = new HashMap<Integer, ArrayList<Integer>>();
	
	@Override
	public void start(Future<Void> fut) 
	{
		HttpServer server = vertx.createHttpServer();
		server
				.requestHandler(r -> {
					// initiate the message map at first
					initMsgMap(15);

					String inputKey = r.getParam("key");
					String msg = r.getParam("message");

					String finalMsg;

					if (inputKey==null||msg==null)
						finalMsg = "\n";
					else{
						Integer miniKey;
						miniKey = keyMap.get(inputKey);
						if(miniKey == null){
							BigInteger keyY = new BigInteger(inputKey);
							miniKey = keyY.divide(secretKey).mod(new BigInteger("25")).add(new BigInteger("1")).intValue(); // get Z
							keyMap.put(inputKey, miniKey);  
						}

						// get the final message with diagonal message and the minikey
						finalMsg = decipherPDC(r.getParam("message"), miniKey); 
					}
					
					Date date = new Date();
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					// response
					r.response().end(teamId + "," + teamAWSId+"\n"+simpleDateFormat.format(date)+"\n"+finalMsg+"\n");
				})
				.listen(80, result -> {
					if (result.succeeded()) {
						fut.complete();
					} else {
						fut.fail(result.cause());
					}
				});
	}

	/**
	 * Decipher using the matrix and the minikey
	 * @param diagMsg
	 * @param miniKey
	 * @return the deciphered text
	 */
	private String decipherPDC(String diagMsg, int miniKey){
		int minikey = miniKey % 26; 
		int i;
		int len = diagMsg.length();
		int tmpAscii;
		char tmpChar = 65;
		
		String msg = "";
		String finalmsg = "";
		
		ArrayList<Integer> idxArr = msgMap.get(len);
		if(idxArr==null){
			idxArr = getIdxArrWithDiameter((int) Math.sqrt(len));
			//System.out.println("	Fresh start!");
		} else {
			//System.out.println("	OLD SCHOOL!");
		}
		
		for(i=0;i<len;i++)
			msg += diagMsg.charAt(idxArr.get(i));
		
		msg = msg.toUpperCase(); // make sure the string is Uppercase
		
		// get the final message with minikey
		for(i=0;i<len;i++)
		{
			tmpAscii = (int) msg.charAt(i);
			if(tmpAscii-minikey<65)
				tmpAscii += 26;
			tmpChar = (char) (tmpAscii-minikey);
			finalmsg = finalmsg + tmpChar;
		}
		return finalmsg;
	}

	/**
	 * Produces the deciphering sequences (index array) of cipher matrices with sizes 
	 * (size: number of elements) from 2 to the diameterUpperBound 
	 * (diameter: as row and column has the same length, diameter stands for either length)
	 * @param diameterUpperBound
	 */
	private void initMsgMap(Integer diameterUpperBound)
	{
		ArrayList<Integer> tmpArr = new ArrayList<Integer>();
		int ite;
		for (ite=2;ite<=diameterUpperBound;ite++){
			getIdxArrWithDiameter(ite);
		}
	}

	/**
	 * Produces the index array (deciphering sequence) of the cipher matrix and put the array into the global msgMap
	 * @param diameter: the diameter of the matrix (as row and column has the same length, diameter stands for either length)
	 * @return ArrayList<Integer>: the index array (deciphering sequence)
	 */
	private ArrayList<Integer> getIdxArrWithDiameter(Integer diameter)
	{
		ArrayList<Integer> tmpArr = new ArrayList<Integer>();
		tmpArr = new ArrayList<Integer>();
		int i,j,k;
		tmpArr.add(0);
		for(i=2;i<=diameter;i++)
		{
			for(j=i;j>0;j--)
			{	
				k = i-j;
				tmpArr.add(k*diameter+j-1);
			}
		}
		// after passing the diameter of the matrix of encoded message
		for(i=diameter-1;i>=1;i--)
		{
			for(j=diameter;j>(diameter-i);j--)
			{	
				k = 2*diameter-i-j;
				tmpArr.add(k*diameter+j-1);
			}
		}
		msgMap.put(tmpArr.size(),tmpArr);
		return tmpArr;
	}

	
}