package team_q2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.sql.Timestamp;

import org.json.JSONObject;

class Item{
	String uid;
	String timestamp;
	
	String tid;
	int score;
	String text;
}

/**
 * Read JSON file
 * 
 * (1) filter time
 * (2) split word
 * (3) calculate score
 * (4) text censoring
 *
 */
public class Mapper {
	private Map<String,Integer> bannedWordMap;
	private Map<String,Integer> sentimentMap;
	
	
	private String filename; //json filename
	
	// time filter :
	//Wed May 07 12:51:12 +0000 2014
	String dateStr =  "Sun Apr 20 00:00:00 +0000 2014";
	String dateStrParsed = dateStr.substring(4, 19)+dateStr.substring(25, 30);
	Date beginDate;
	
	public Mapper(){
		
		try {
			beginDate = new SimpleDateFormat("MMM dd hh:mm:ss yyyy").parse(dateStrParsed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Builder builder = new Builder("banned.txt","afinn.txt");
		this.bannedWordMap = builder.getBannedWordMap();
		this.sentimentMap = builder.getSentimentMap();
	}
	
	/**
	 * read directly from System.in
	 * 
	 * http://theoryapp.com/parse-json-in-java/
	 * @throws FileNotFoundException 
	 */
	public void readJson() throws FileNotFoundException{
		//FileReader fileReader = new FileReader("output.json");
		FileReader fileReader = new FileReader("top1000.json");		
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		BufferedReader br = new BufferedReader(fileReader);
	//	PrintWriter pr = new PrintWriter("output10.json");
		
	    String line;
	    int lineNum = 0;
	    try {
			while ((line=br.readLine())!=null) {
				int score = 0;
			    JSONObject obj = new JSONObject(line);
			   
			    JSONObject userObj = obj.getJSONObject("user");
			    
			    String text = obj.getString("text");
			    String id = obj.getString("id_str");
			    String user_id = userObj.getString("id_str");
			    
			    
			    // time filter
			    String timeStr = obj.getString("created_at");
			    
				String timeStrParsed = timeStr.substring(4, 19)+timeStr.substring(25, 30);
			    
			    Date currentTime = new SimpleDateFormat("MMM dd hh:mm:ss yyyy").parse(timeStrParsed);
			    
			    if(currentTime.after(beginDate)){
				    //System.out.println(id + " " + text);
				    
				    //split with non alpha numerical
				    String[] str = text.split("\\P{Alnum}+");
				    
				    // calculate score
				    for(String s : str){
				    	if(this.sentimentMap.containsKey(s.toLowerCase())){
				    		int tempScore = this.sentimentMap.get(s.toLowerCase());
				    		score += tempScore;
				    	}
				    }
				   // censoring Text
				    for(String s: str){
				    	if(this.bannedWordMap.containsKey(s.toLowerCase())){
				    		String repeated = new String(new char[s.length()-2]).replace("\0", "*");
				    		String replacement = s.charAt(0)+ repeated+s.charAt(s.length()-1);
				    		text = text.replaceAll(s, replacement);
				    	}
				    }
				    
				    String outputJSONObj = new JSONObject()
				    					   .put("id", id)
				    					   .put("score", score)
				    					   .put("cleanText", text).toString();
				    
				    System.out.println(user_id+"_"+timeStr+"\t"+outputJSONObj);     
				  //  pr.flush();
				   // lineNum++;
				    //System.out.println(lineNum);
			    }
			    	    
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    //pr.close();
	}
	
	public static void main(String[] args) throws FileNotFoundException{
		Mapper mapper = new Mapper();
		mapper.readJson();
	}
}
