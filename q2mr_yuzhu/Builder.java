import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * banned.txt => decipher => HashMap<String,Integer>
 * afinn.txt => HashMap<String,Integer>
 * 
 */
public class Builder {
	private String bannedWordFile;
	private String sentimentScoreFile;
	
	private Map<String,Integer> bannedWordMap;
	private Map<String,Integer> sentimentMap;
	
	public Builder(String bannedWordFile,String sentimentScoreFile){
		this.bannedWordFile = bannedWordFile;
		this.sentimentScoreFile = sentimentScoreFile;
		
		this.bannedWordMap = new HashMap<String,Integer>();
		this.sentimentMap = new HashMap<String,Integer>();
		
		buildBannedWordMap();
		buildSentimentMap();
	}
	
	/**
	 * 15619ppgrfg -> 15619cctest
	 * 
	 * replace with a char after it with distance 13
	 * a->n,b->o ... m->z
	 * n->a,o->b ... z->m
	 * 
	 */
	public void buildBannedWordMap(){
        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
        	
            // Always wrap FileReader in BufferedReader.   
            
			InputStream in = new URL(bannedWordFile).openConnection().getInputStream();
			
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            
            while((line = bufferedReader.readLine()) != null) {
//                System.out.println(line+"  "+decipherROT13ed(line));
            	bannedWordMap.put(decipherROT13ed(line), 1);
            }   

            // Always close files.
            bufferedReader.close();         
        }catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                		bannedWordFile + "'");                
        }catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + bannedWordFile + "'");
        }		
	}
	
	/**
	 * delimiter is tab
	 */
	public void buildSentimentMap(){
		 // This will reference one line at a time
        String line = null;

        try {
            
			InputStream in = new URL(sentimentScoreFile).openConnection().getInputStream();
			
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

            while((line = bufferedReader.readLine()) != null) {
            	String[] segs = line.split("\t");
            	
            	sentimentMap.put(segs[0], Integer.parseInt(segs[1]));
            }   
            
//            for(Map.Entry<String, Integer> entry : sentimentMap.entrySet()){
//            	System.out.println(entry.getKey()+"  "+entry.getValue());
//            }

            // Always close files.
            bufferedReader.close();         
        }catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                		bannedWordFile + "'");                
        }catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + bannedWordFile + "'");
        }		
		
	}
	
	private String decipherROT13ed(String origin){
		char[] arr = origin.toCharArray();
		int len = arr.length;
		int temp;
		
		for(int i=0;i<len;++i){
			temp = (int)arr[i];
			
			// is a-z
			if(temp>= 97 && temp<= 122){
				if((int)arr[i] > 109){ //'m'
					arr[i] = (char)( temp - 13);
				}else{
					arr[i] = (char)( temp + 13);
				}
			}
		}
		
		return new String(arr);
	}
	
	
	public Map<String,Integer> getBannedWordMap(){
		return this.bannedWordMap;
	}
	
	public Map<String,Integer> getSentimentMap(){
		return this.sentimentMap;
	}
	
	/**
	 * For testing
	 * @param args
	 */
	public static void main(String[] args){
		Builder builder = new Builder("banned.txt","afinn.txt");
//		builder.buildBannedWordMap();
		
		builder.buildSentimentMap();
	}
}
