import org.json.JSONObject;

public class JSONItem implements Comparable<JSONItem>{
	public JSONObject obj;
	public String key;
	
	public JSONItem(String key,JSONObject obj){
		this.key = key;
		this.obj = obj;
	}
	@Override
	public int compareTo(JSONItem o) {
		// TODO Auto-generated method stub
		return (int)(Long.parseLong(this.obj.getString("id"))- Long.parseLong(o.obj.getString("id")));
	}
}
