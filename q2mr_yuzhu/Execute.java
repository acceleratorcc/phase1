import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.json.JSONObject;


public class Execute {
  
  public static class JSONMapper extends Mapper<Object, Text, Text, Text>{
	
	
    public static Map<String,Integer> bannedWordMap;
    public static Map<String,Integer> sentimentMap;
   
    //String filename; //json filename

	// time filter :
    //Wed May 07 12:51:12 +0000 2014
    
    public static Date beginDate;
    public static String dateStr;
    public static String dateStrParsed;
    
    
	public Builder builder;
	
	public void setup(Context context){
		
		dateStr =  "Sun Apr 20 00:00:00 +0000 2014";
		dateStrParsed = dateStr.substring(4, 19)+dateStr.substring(25, 30);
		
		try {
			beginDate = new SimpleDateFormat("MMM dd hh:mm:ss yyyy").parse(dateStrParsed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		builder = new Builder("https://cmucc-datasets.s3.amazonaws.com/15619/f15/banned.txt","https://cmucc-datasets.s3.amazonaws.com/15619/f15/afinn.txt");
		
		bannedWordMap = builder.getBannedWordMap();
		sentimentMap = builder.getSentimentMap();
		
	}
	
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
    	
	    String line = value.toString();
	    //int lineNum = 0;
	    try {
				int score = 0;
			    JSONObject obj = new JSONObject(line);
			   
			    JSONObject userObj = obj.getJSONObject("user");
			    
			    String text = obj.getString("text");
			    String id = obj.getString("id_str");
			    String user_id = userObj.getString("id_str");
			    
			    // time filter
			    String timeStr = obj.getString("created_at");
			    
				String timeStrParsed = timeStr.substring(4, 19)+timeStr.substring(25, 30);
			    
			    Date currentTime = new SimpleDateFormat("MMM dd hh:mm:ss yyyy").parse(timeStrParsed);
			    
			    if(currentTime.after(beginDate)){
				    //System.out.println(id + " " + text);
				    
				    //split with non alpha numerical
				    String[] str = text.split("\\P{Alnum}+");
				    
				    // calculate score
				    for(String s : str){
				    	if(sentimentMap.containsKey(s.toLowerCase())){
				    		int tempScore = sentimentMap.get(s.toLowerCase());
				    		score += tempScore;
				    	}
				    }
				   // censoring Text
				    for(String s: str){
				    	if(bannedWordMap.containsKey(s.toLowerCase())){
				    		String repeated = new String(new char[s.length()-2]).replace("\0", "*");
				    		String replacement = s.charAt(0)+ repeated+s.charAt(s.length()-1);
				    		text = text.replaceAll(s, replacement);
				    	}
				    }
				    
				    String outputJSONObj = new JSONObject()
				    					   .put("id", id)
				    					   .put("score", score)
				    					   .put("cleanText", text).toString();
				    
				    context.write(new Text(user_id+"_"+timeStr),new Text(outputJSONObj));     
				  //  pr.flush();
				   // lineNum++;
				    //System.out.println(lineNum);
			    }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    //pr.close();
    }
    
  }
  //mapper key value, reducer key value
  public static class JSONReducer extends Reducer<Text,Text,Text,Text> {

    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
    	
      String line = null;
  	  ArrayList<JSONItem> buffer = new ArrayList<JSONItem>();
  	  String oldKey = null;
  	
      for (Text val : values) {
    	  
    	line = val.toString();
    	
    	String JSONObjStr = line.substring(line.indexOf("{"), line.length());
    	JSONObject currentobj = new JSONObject(JSONObjStr);
    	
    	String currentKey = key.toString();
    	
		buffer.add(new JSONItem(currentKey,currentobj));
      }
      
  	  Collections.sort(buffer);
  	  
  	  JSONItem oldItem = null;
  	  
  	  for(JSONItem jItem: buffer){
		if((oldItem!=null) && jItem.compareTo(oldItem)!= 0 ){
			context.write(key, new Text(oldItem.obj.toString()));
		}
		oldItem = jItem;
	  }
  	  
	  if(oldItem!=null){
		  context.write(key, new Text(oldItem.obj.toString()));
	  }
	  
	  buffer.clear();
      
    }
    
  }
  

  public static void main(String[] args) throws Exception {
	
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "JSON MR");
    job.setJarByClass(Execute.class);
    job.setMapperClass(JSONMapper.class);
    job.setReducerClass(JSONReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
  
}