
有用的只有 builder execute 和 jsonitem， 另外的两个java你们可以用来检查一下execute逻辑， jsonmr是我编译过得
我参考的是word count： https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html

1.
要用1.7 的编译，可以在emr上弄 ，hadoop 那两个要包括进来
javac -cp ./json-20150729.jar:./hadoop-core-1.2.1.jar:./hadoop-common-2.6.0.jar *.java


现在这块编译的是由warning， son的版本是52， 你们看看网上有没有51的，据说52是用1.8编译的， 咱们要找1，7的

warning: ./json-20150729.jar(org/json/JSONObject.class): major version 52 is newer than 51, the highest major version supported by this compiler.
  It is recommended that the compiler be upgraded.
warning: ./json-20150729.jar(org/json/JSONTokener.class): major version 52 is newer than 51, the highest major version supported by this compiler.
  It is recommended that the compiler be upgraded.
warning: ./json-20150729.jar(org/json/JSONException.class): major version 52 is newer than 51, the highest major version supported by this compiler.
  It is recommended that the compiler be upgraded.


2.

把生成的三个class 合成一个jar
jar cvf JSONMR.jar *.class

added manifest
adding: Builder.class(in = 3021) (out= 1522)(deflated 49%)
adding: Execute.class(in = 1398) (out= 774)(deflated 44%)
adding: Execute$JSONMapper.class(in = 4467) (out= 2092)(deflated 53%)
adding: Execute$JSONReducer.class(in = 2364) (out= 1119)(deflated 52%)
adding: JSONItem.class(in = 789) (out= 439)(deflated 44%)


3.
把合成的jar 放在 s3上， 建一个bucket里面开folder， 记路径
把input file： top1000.json 放s3上记录经

4. emr 改这两个

export HADOOP_CLASSPTH=/home/hadoop/testMR/json-20150729.jar
export HADOOP_USER_CLASSPATH_FIRST=“true"

5.
把 json库放到emr上，记下路径

6.
emr放好了东西可以加step了

step type选 custom jar

jar location为 s3 上的 jar路径

argument 如下

Execute s3://testmrinput/myinput/ s3://testmroutput/myoutput2/ -libjars /home/hadoop/testMR/json-20150729.jar


Execute 为主类
s3://testmrinput/myinput/ 这个为input的路径

s3://testmroutput/myoutput2/ output路径，必须是之前不存在的

-libjars /home/hadoop/testMR/json-20150729.jar ， -libjars后面为emr上的json库路径

我对argument 这块存疑，几个参数顺序有问题，你们查一下 custom jar包 怎么加argument

可以参考 https://docs.google.com/document/d/13o05C29B32YSd9GufBPZ8fLPXlZ0CbTjJVHbNKkE0-4/edit#

这是最后报的错，好像是jsonobj没找到， -libjars的问题


Error: java.lang.ClassNotFoundException: org.json.JSONObject
	at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
	at Execute$JSONMapper.map(Execute.java:71)
	at Execute$JSONMapper.map(Execute.java:28)
	at org.apache.hadoop.mapreduce.Mapper.run(Mapper.java:152)
	at org.apache.hadoop.mapred.MapTask.runNewMapper(MapTask.java:793)
	at org.apache.hadoop.mapred.MapTask.run(MapTask.java:342)
	at org.apache.hadoop.mapred.YarnChild$2.run(YarnChild.java:171)
	at java.security.AccessController.doPrivileged(Native Method)
	at javax.security.auth.Subject.doAs(Subject.java:415)
	at org.apache.hadoop.security.UserGroupInformation.doAs(UserGroupInformation.java:1628)
	at org.apache.hadoop.mapred.YarnChild.main(YarnChild.java:166)

Error: java.lang.ClassNotFoundException: org.json.JSONObject
	at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
	at Execute$JSONMapper.map(Execute.java:71)
	at Execute$JSONMapper.map(Execute.java:28)
	at org.apache.hadoop.mapreduce.Mapper.run(Mapper.java:152)
	at org.apache.hadoop.mapred.MapTask.runNewMapper(MapTask.java:793)
	at org.apache.hadoop.mapred.MapTask.run(MapTask.java:342)
	at org.apache.hadoop.mapred.YarnChild$2.run(YarnChild.java:171)
	at java.security.AccessController.doPrivileged(Native Method)
	at javax.security.auth.Subject.doAs(Subject.java:415)
	at org.apache.hadoop.security.UserGroupInformation.doAs(UserGroupInformation.java:1628)
	at org.apache.hadoop.mapred.YarnChild.main(YarnChild.java:166)

Container killed by the ApplicationMaster.
Container killed on request. Exit code is 143
Container exited with a non-zero exit code 143

Error: java.lang.ClassNotFoundException: org.json.JSONObject
	at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
	at Execute$JSONMapper.map(Execute.java:71)
	at Execute$JSONMapper.map(Execute.java:28)
	at org.apache.hadoop.mapreduce.Mapper.run(Mapper.java:152)
	at org.apache.hadoop.mapred.MapTask.runNewMapper(MapTask.java:793)
	at org.apache.hadoop.mapred.MapTask.run(MapTask.java:342)
	at org.apache.hadoop.mapred.YarnChild$2.run(YarnChild.java:171)
	at java.security.AccessController.doPrivileged(Native Method)
	at javax.security.auth.Subject.doAs(Subject.java:415)
	at org.apache.hadoop.security.UserGroupInformation.doAs(UserGroupInformation.java:1628)
	at org.apache.hadoop.mapred.YarnChild.main(YarnChild.java:166)

Container killed by the ApplicationMaster.
Container killed on request. Exit code is 143
Container exited with a non-zero exit code 143


