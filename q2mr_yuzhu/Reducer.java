package team_q2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONObject;

public class Reducer {
	
	public void readMapperOutput() throws FileNotFoundException{
		
		//FileReader fileReader = new FileReader("output.json");
		//FileReader fileReader = new FileReader("output1000KeySorted.json");		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		//BufferedReader br = new BufferedReader(fileReader);
		
		//PrintWriter pr = new PrintWriter("output1000Sorted.json");
		
		String line;
		String oldKey = null;
		//int lineNum =0;
		ArrayList<JSONItem> buffer = new ArrayList<JSONItem>();
		try{
			while((line=br.readLine())!=null){
				String JSONObjStr = line.substring(line.indexOf("{"), line.length());
				
				JSONObject currentobj = new JSONObject(JSONObjStr);
				
				String currentKey = line.substring(0, line.indexOf("{")-1);
				
				if((oldKey!=null)&&(oldKey.compareTo(currentKey)!=0)){
					Collections.sort(buffer);
					
					JSONItem oldItem = null;
					
					for(JSONItem jItem: buffer){
						if((oldItem!=null) && jItem.compareTo(oldItem)!= 0 ){
							System.out.println(oldKey+"\t"+oldItem.obj.toString());
							//pr.println(oldKey+"\t"+oldItem.obj.toString());
							//pr.flush();
							//System.out.println(lineNum);
						}
						oldItem = jItem;
					}
					if(oldItem!=null){
						System.out.println(oldKey+"\t"+oldItem.obj.toString());
						//pr.println(oldKey+"\t"+oldItem.obj.toString());
						//pr.flush();
						//System.out.println(lineNum);
					}
					buffer.clear();
				}
				
				oldKey = currentKey;
				buffer.add(new JSONItem(currentKey,currentobj));
				//lineNum++;
				
			}
			if(oldKey!= null){
				
				Collections.sort(buffer);
				
				JSONItem oldItem = null;
				
				for(JSONItem jItem: buffer){
					if((oldItem!=null) && jItem.compareTo(oldItem)!= 0){
						System.out.println(oldKey+"\t"+oldItem.obj.toString());
						//pr.println(oldKey+"\t"+oldItem.obj.toString());
					}
					oldItem = jItem;
				}
				if(oldItem!=null){
					System.out.println(oldKey+"\t"+oldItem.obj.toString());
					//pr.println(oldKey+"\t"+oldItem.obj.toString());
				}
				buffer.clear();
			}
			//pr.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) throws FileNotFoundException{
		Reducer reducer = new Reducer();
		reducer.readMapperOutput();
	}
}
