package reference;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* F15 15619 Project 1.2
 * Date: 2015 Sept 19
 * Author: Yilei CHU
 * 
 * original line structure:
 * [project name] [page title] [number of accesses] [total data returned in bytes]
 * 
 * Project 1.1 Rules:
 * Rule 1: [project name] must be "en", no suffix like .b is allowed
 * Rule 2: Title could not start with the following:
 * 	Media:
	Special: Talk: User: User_talk: Project: Project_talk: File: File_talk:
	MediaWiki: MediaWiki_talk: Template: Template_talk: Help: Help_talk: Category: Category_talk:
	Portal: Wikipedia: Wikipedia_talk:
	
 * Rule 3: Title could not start with lowercase English characters (non-English letters like digits are allowed)
 * Rule 4: Title could not end with the following: (case sensitive)
 * 			.jpg, .gif, .png, .JPG, .GIF, .PNG, .txt, .ico
 * Rule 5: Title could not eactly be the following:
 * 			404_error/
			Main_Page
			Hypertext_Transfer_Protocol
			Search
 * -----------------------------------------------------------------------------------------------		
 * Project 1.2 Additional Rules
 * Rule 6: Malformed entries are entries with missing article name. Make sure that you are filtering these entries.
 * 
 * Tips on extracting the date info:
	Getting the input filename from within a Mapper: As the date/time information is encoded in the filename, Hadoop streaming makes the filename available to every map task through the environment variables mapreduce_map_input_file. For example, the filename can be accessed in python using the statement os.environ["mapreduce_map_input_file"], or in Java using the statement System.getenv("mapreduce_map_input_file")
	Aggregate the pageviews from hourly views to daily views.
	
	
	Calculate the total pageviews for each article.
	For every article that has page-views over 100,000, print the following line as output (\t is the tab character):
	[total month views]\t[article name]\t[date1:page views for date1]\t[date2:page views for date2]... 
	
	
 */
class Item{
	String title;
	int accessNo;
	boolean valid;
	
	public Item(boolean valid){
		this.title = "";
		this.accessNo = 0;
		this.valid = valid;
	}
	
	public Item(String title,int accessNo,boolean valid){
		this.title = title;
		this.accessNo = accessNo;
		this.valid = valid;
	}
}


public class Mapper {
	
	private String todayDate;
	private String[] InvalidPrefix = {	"Media:",
										"Special:",
										"Talk:",
										"User:",
										"User_talk:",
										"Project:",
										"Project_talk:",
										"File:",
										"File_talk:",
										"MediaWiki:",
										"MediaWiki_talk:",
										"Template:",
										"Template_talk:",
										"Help:",
										"Help_talk:",
										"Category:",
										"Category_talk:",
										"Portal:",
										"Wikipedia:",
										"Wikipedia_talk:"};
	
	private String[] InvalidExtention = {
			".jpg", ".gif", ".png", ".JPG", ".GIF", ".PNG", ".txt", ".ico"
	};
	
	private String[] InvalidTitle = {
			"404_error/",
			"Main_Page",
			"Hypertext_Transfer_Protocol",
			"Search"
	};
	
	/* (1) Use the aws s3 cli to see filename format
	 * 			aws s3 ls s3://cmucc-datasets/wikipediatraf/201508/ --recursive --human-readable --summarize
	 * (2) filename format: 
	 * 			wikipediatraf/201508/pagecounts-20150831-230000.gz
	 * (3) extract date info from file name by index of "-201508"
	 */
	public Mapper(){
		String filename = System.getenv("mapreduce_map_input_file");
		int datepos =  filename.lastIndexOf("201508");
		
		if(datepos == -1){
			System.err.println("Attention: Filename not correct!!!!");
			System.exit(38); //customized exit status 
		}else if(datepos+8 > filename.length()){
			System.err.println("Attention: Filename too short to contain a date info!!!!");
			System.exit(39); //customized exit status 
		}
		
		/* For example: 20150831*/
		this.todayDate = filename.substring(datepos, datepos+8);
	}
	
	public void readFile() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
	    String line;
	    /* Avoid occupying too much memory. Process a line and output immediately */
	    try {
			while ((line=br.readLine())!=null) {
			    /* process the line */
				Item res = checkLine(line);
				if(res.valid){	    		
					/* print out the result 
					 * format: <title> <date> <access #> */
					
					System.out.println(res.title+"\t"+todayDate+"\t"+res.accessNo);    
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** Test the input text line should be filtered out or not
	 * @param line the input text line o structure defined in header
	 * @return
	 */
	private Item checkLine(String line){
		/* Rule 1 : project name */
		int firstSpace = line.indexOf(" ");
		String projName = line.substring(0, firstSpace);		
		if(!projName.equals("en")){
			return new Item(false);
		}
		
		/* Extract title between the second and the third space */
		int secondSpace = line.indexOf(" ",firstSpace+1);
		String title = line.substring(firstSpace+1,secondSpace);
		
		/* Project 1.2 New Rule 6: delete entries with missing article name */
		if(title == null || title.isEmpty() || title.trim().isEmpty()){
			return new Item(false);
		}
		
		/* Rule 3: title not start with lower case letter */
		if(!title.isEmpty() && Character.isLowerCase(title.charAt(0))){
			return new Item(false);
		}
		
		/* Rule 5: title not exact matching */
		for(int i=0;i<InvalidTitle.length;++i){
			if(title.equals(InvalidTitle[i])){
				return new Item(false);
			}
		}
		
		/* Rule 2: title start with */
		for(int i=0;i<InvalidPrefix.length;++i){
			if(title.startsWith(InvalidPrefix[i])){
				return new Item(false);
			}
		}
		
		/* Rule 4: title suffix */
		for(int i=0;i<InvalidExtention.length;++i){
			if(title.endsWith(InvalidExtention[i])){
				return new Item(false);
			}
		}
		
		/* extract number of access */
		int thirdSpace = line.indexOf(" ",secondSpace+1);
		int accessNo = Integer.parseInt(line.substring(secondSpace+1,thirdSpace));
		
		return new Item(title,accessNo,true);
	}
	
	public static void main(String[] args){
		Mapper mapper = new Mapper();
		mapper.readFile();
	}
	
}
