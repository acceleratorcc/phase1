package reference;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/* F15 15619 Project 1.2 Reducer part
 * Date: 2015 Sept 19
 * Author: Yilei CHU
 * 
 * Reference: wordcount_reducer.java
 * 
 */

public class Reducer {
    
    
    public static void main (String args[]) {
     
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        /* Initialize Variables */
        String line;
        
        String prevTitle = null;
        String currentTitle = null;
        
        int monthlyCount = 0; // sum of daily count for the title (containing 31 days)
        
        int[] dailyRecord = new int[32]; // record daily sum for a title in a month, day1 at index 1 rather than 0
        Arrays.fill(dailyRecord, 0);
        
        int threshold = 100000;
        int modBase = 20150800;
        int date;
        int accessNo;
        
        String[] dateStrs = new String[32]; // 20150800 - 20150831
        
        for(int i=0;i<32;++i){
            dateStrs[i] = Integer.toString(i+modBase);
        }
        
        /* While having stdin input */
        try {
            while((line=br.readLine())!=null){
                /* Parse the line using format:  <title> <date> <access #> */
                String[] parts = line.split("\t");
                currentTitle = parts[0];
                date = Integer.parseInt(parts[1]) % modBase; // 20150831 -> 31
                accessNo = Integer.parseInt(parts[2]);
                
                /* check if same title */
                if(prevTitle!=null && currentTitle.equals(prevTitle)){ //same title, aggregate
                    dailyRecord[date] += accessNo;
                    monthlyCount += accessNo;
                }else //The title has changed
                {
                    //this isn't the first title, output previous result if > 100,000 
                    if(prevTitle != null) 
                    {
                        if(monthlyCount > threshold){
                            /* print out format 
                             * [total month views]\t[article name]\t[date1:page views for date1]\t[date2:page views for date2]
                             */
                            
                            String output = Integer.toString(monthlyCount)+"\t"+prevTitle;
                            
                            for(int i=1;i<32;++i){ //from index 1
                                output = output + "\t" + dateStrs[i]+":"+ //datei:
                                            Integer.toString(dailyRecord[i]);   //page views for datei
                            }
                            
                            System.out.println(output);                         
                        }
                        
                        // do the initialization for next title if any
                        Arrays.fill(dailyRecord, 0);
                        monthlyCount = 0;
                    }
                    
                    // currentTitle == null,this is the first title
                    prevTitle = currentTitle;
                    
                    // record down the first data for current title
                    dailyRecord[date] += accessNo;
                    monthlyCount += accessNo;
                }    
            }

            /* print out result for last current title */
            if(currentTitle != null && monthlyCount > threshold){
                /* print out format 
                 * [total month views]\t[article name]\t[date1:page views for date1]\t[date2:page views for date2]
                 */
                
                String output = Integer.toString(monthlyCount)+"\t"+currentTitle;
                
                for(int i=1;i<32;++i){ //from index 1
                    output = output + "\t" + dateStrs[i]+":"+ //datei:
                                Integer.toString(dailyRecord[i]);   //page views for datei
                }
                
                System.out.println(output);                         
            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
