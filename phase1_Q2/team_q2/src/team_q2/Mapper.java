package team_q2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.json.JSONObject;

class Item{
	String uid;
	String timestamp;
	
	String tid;
	int score;
	String text;
}

/**
 * Read JSON file
 * 
 * (1) filter time
 * (2) split word
 * (3) calculate score
 * (4) text censoring
 *
 */
public class Mapper {
	private Map<String,Integer> bannedWordMap;
	private Map<String,Integer> sentimentMap;
	
	private String filename; //json filename
	
	public Mapper(){
		Builder builder = new Builder("banned.txt","afinn.txt");
		this.bannedWordMap = builder.getBannedWordMap();
		this.sentimentMap = builder.getSentimentMap();
	}
	
	/**
	 * read directly from System.in
	 * 
	 * http://theoryapp.com/parse-json-in-java/
	 * @throws FileNotFoundException 
	 */
	public void readJson() throws FileNotFoundException{
		FileReader fileReader = new FileReader("output.json");
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		BufferedReader br = new BufferedReader(fileReader);
		
	    String line;
	    
	    try {
			while ((line=br.readLine())!=null) {
			    JSONObject obj = new JSONObject(line);
			    String text = obj.getString("text");
			    String id = obj.getString("id_str");
			    System.out.println(id + " " + text);  
			}
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
	
	
	
	
	public static void main(String[] args) throws FileNotFoundException{
		Mapper mapper = new Mapper();
		mapper.readJson();
	}
}
